package ru.neoflex.route;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.model.rest.RestBindingMode;
import org.apache.camel.model.rest.RestParamType;
import ru.neoflex.model.Agreement;
import ru.neoflex.model.Client;

public class MainRoute extends RouteBuilder {

    @Override
    public void configure() throws Exception {

        restConfiguration()
                .bindingMode(RestBindingMode.json);

        rest("/client")
                .post()
                .id("REST POST | /client")
                .description("Create client").type(Client.class)
                .responseMessage().code(201).endResponseMessage()
                .to("direct:route-operation-b");

        rest("/client/agreement")
                .get()
                .id("REST GET | /client/agreement")
                .description("Get agreement by client id").outType(Agreement[].class)
                .responseMessage().code(200).example("value", "10").endResponseMessage()
                .param().name("clientId").type(RestParamType.query).endParam()
                .to("direct:route-operation-a");

        from("direct:route-operation-b")
                .marshal(new JacksonDataFormat())
                .removeHeaders("CamelHttp*")
                .to("http://microservice-b-v1/api/client")
                .unmarshal(new JacksonDataFormat())
                .setBody(simple(null))
                .setHeader(Exchange.HTTP_RESPONSE_CODE, simple("201"))
                .to("log:route-operation-b?marker=messageOut.input&showAll=true");

        from("direct:route-operation-a")
                .setBody(simple("${header.clientId}"))
                .to("jms:queue:q-request-oper-a?replyTo=q-response-oper-a&exchangePattern=InOut&replyToType=Exclusive")
                .unmarshal(new JacksonDataFormat())
                .to("log:route-operation-a?marker=messageOut.input&showAll=true");
    }
}
