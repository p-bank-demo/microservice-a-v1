FROM maven:3.9-eclipse-temurin-20 as build
WORKDIR /home/app
COPY . .
RUN mvn -f pom.xml clean package -Dmaven.test.skip -Dquarkus.package.type=uber-jar

FROM openjdk:20-slim as run
COPY --from=build /home/app/target/microservice-a-0.0.1-runner.jar /app/microservice-a-0.0.1-runner.jar

EXPOSE 8080
ENTRYPOINT ["java","-jar","/app/microservice-a-0.0.1-runner.jar"]